import xlrd
import xml
from xml.dom.minidom import Document
import xml.etree.ElementTree as ET
from lxml import etree as ET
import json


def open_excel ( file = 'file.xls'):
    try:
        data = xlrd.open_workbook( file )
        return data
    except Exception:
        print (str(e))

def ExportXML ( file = 'file.xlsx', sheetIndex = 0 ):
	fullData = open_excel(file)
	data = fullData.sheet_by_index(sheetIndex)
	nrows = data.nrows  #行数
	ncols = data.ncols  #列数

#-------------------
	#循环遍历所有的行，并且打印每一行的数据
	# for row in range (rows):
	# 	value = sheet.row_values(row)
	# 	print(value)

	# print("-----------------------")

	# #循环遍历所有的lie，并且打印每一列的数据
	# for col in range (cols):
	# 	value = sheet.col_values(col)
	# 	print(value)
#-------------------


	#循环遍历所有的行，并且打印每一行的数据
	# for row in range (0, nrows):
	# 	rowData = data.row_values(row)
	# 	print (rowData)
	# 	if rowData :
	# 		for i in range(0, len(rowData)):
	# 			print (rowData[i])

	# print("-----------------------")

	#循环遍历所有的列，并且打印每一列的数据
	# for col in range (0, ncols):
	# 	colData = data.col_values(col)
	# 	print (colData)
	# 	if colData:
	# 		for i in range(0, len(colData)):
	# 			print (colData[i])

	# doc = Document()  #创建DOM文档对象
	# root = doc.createElement('Root') #创建根元素
	# doc.appendChild(root)

	# for row in range (1, nrows):
	# 	rowData = data.row_values(row)
	# 	if rowData :
	# 		parent = doc.createElement(rowData[0])
	# 		root.appendChild(parent)

	# 		for i in range (1, len(rowData)):
	# 			language = doc.createElement(data.row_values(0)[i])
	# 			value = doc.createTextNode(rowData[i])
	# 			language.appendChild(value)
	# 			parent.appendChild(language)

	# print(doc.toprettyxml(indent = ''))

	root = ET.Element("root")
	for row in range (1, nrows):
		rowData = data.row_values(row)
		if rowData :
			key = ET.SubElement(root, rowData[0])
			for i in range (1, len(rowData)):
				language = ET.SubElement(key, data.row_values(0)[i])
				language.text = rowData[i]

	tree = ET.ElementTree(root)
	tree.write('cities.xml', pretty_print=True,xml_declaration=True, encoding='UTF-8')

def ExportJson (file = 'file.xlsx', sheetIndex = 0 ):
	fullData = open_excel(file)
	data = fullData.sheet_by_index(sheetIndex)
	nrows = data.nrows  #行数
	ncols = data.ncols  #列数
	root = ET.Element("root")
	for row in range (1, nrows):
		rowData = data.row_values(row)
		if rowData :
			key = ET.SubElement(root, rowData[0])
			for i in range (1, len(rowData)):
				language = ET.SubElement(key, data.row_values(0)[i])
				language.text = rowData[i]

def main():
	#文件路径和文件名
	filename = 'D:\PythonProjects\Test\Test\Test.xlsx'
	ExportXML(filename)

if __name__ == "__main__":
	main()
