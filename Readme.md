##Readme

利用 Python + xlrd 解析 excel 生成 xml 文件。

##主要功能
- 解析 excel
- 读取 excel 数据
- 利用数据生成 xml


##主要作用
- 策划在 excel 中配置游戏数据后
- 利用工具导出成 xml 文件
- 在 unity 中读取 xml 文件
- 游戏中应用策划填写的数据